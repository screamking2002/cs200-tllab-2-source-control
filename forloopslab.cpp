#include <iostream>
#include <string>
using namespace std;

// I completed all the programs. Please look at Program 3 and 4 comments

void Program1()
{
    int i;

    for (i = 0; i < 21; i++)
    {
        cout << i << " ";
    }
}

void Program2()
{
    int counter;

    for (counter = 1; counter <= 128; counter *= 2)
    {
        cout << counter << " ";
    }
}
// I struggled with this program. I eventually figured it out(I think)
void Program3()
{
    int n;
    int counter = 1;
    int sum;

    cout << "Enter a value for n: ";
    cin >> n;
    cout << "\n";

    for (sum = 0; counter <= n; counter++)
    {
        sum += counter; //this was my hang up. The cout i did sum + counter and
        // it didnt work. It would not accept sum += counter in the cout.
        cout << "Sum: " << sum << endl;
    }
}
void Program4()
{
    string text;
    char lookForLetter;
    int letterCount;

    cout << "Enter a string: ";
    cin.ignore();
    getline(cin, text);

    cout << "Enter a letter to count: ";
    cin >> lookForLetter;
    letterCount = 0;


    int i;
    for (i = 0; i < text.size(); i++)
    {
        cout << "Letter " << i << ": " << text[i] << endl;

        if (text[i] == lookForLetter)
        {
            letterCount++;
        }
    }
    cout << "\n"; // I noticed that I matched perfectly with the example in the PDF
    // the problem is the example in the assignment on canvas the program counts the capitol I, 
    //this program does not like the example in the pdf doc. strcasecmp() or tolower would work?
    cout << "There are " << letterCount << " " << lookForLetter << "(s) in \"" << text << "\"";

}

int main()
{
    // Don't modify main
    while ( true )
    {
        cout << "Run which program? (1-4): ";
        int choice;
        cin >> choice;

        cout << endl << endl;

        if      ( choice == 1 ) { Program1(); }
        else if ( choice == 2 ) { Program2(); }
        else if ( choice == 3 ) { Program3(); }
        else if ( choice == 4 ) { Program4(); }

        cout << endl << "------------------------------------" << endl;
    }

    return 0;
}
